package org.bitbucket._newage.betteralias;

import java.util.HashMap;

import org.bitbucket._newage.betteralias.event.NatureEvent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.ne0nx3r0.betteralias.BetterAlias;

public class NatureCommandExecutor implements CommandExecutor {

	private final BetterAlias plugin;
	private HashMap<Player, Long> hm = new HashMap<Player, Long>();
	
	public NatureCommandExecutor(BetterAlias plugin) {
		this.plugin = plugin;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdAlias, String[] args) {
		if(cmdAlias.equals("priroda") || cmdAlias.equals("rtp"))
			if(sender instanceof Player) {
				Player p = (Player) sender;
				if(!p.hasPermission("betteralias.priroda")) {
					sender.sendMessage(ChatColor.RED+"Nemas opravneni pouzit tento prikaz!");
					return true;
				}
				else if(hm.get(p) == null || (System.currentTimeMillis()-hm.get(p))>120000) {
					if(hm.containsKey(p))
						hm.remove(p);
					hm.put(p, System.currentTimeMillis());
					plugin.getServer().getPluginManager().callEvent(new NatureEvent(p));
					return true;
				} else {
					sender.sendMessage(ChatColor.RED+"Musis pockat jeste "+(120-(System.currentTimeMillis()-hm.get(p))/1000)+" sekund");
					return false;
				}

			}
		return false;
	}

}
