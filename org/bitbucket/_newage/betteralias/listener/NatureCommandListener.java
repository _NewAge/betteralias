package org.bitbucket._newage.betteralias.listener;

import org.bitbucket._newage.betteralias.event.NatureEvent;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.ne0nx3r0.betteralias.BetterAlias;
import com.ne0nx3r0.betteralias.alias.Alias;

public class NatureCommandListener implements Listener {
	private final BetterAlias plugin;
	
	public NatureCommandListener(BetterAlias plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority=EventPriority.LOWEST, ignoreCancelled = true)
	public void onNatureCommandExecute(NatureEvent e) {
		
        String sCommand = "cpriroda";

        Alias alias = plugin.aliasManager.getAliasMatch(sCommand);
        
        if(alias != null)
        {     
            String sArgs = sCommand.substring(alias.command.length());
        
            if(plugin.aliasManager.sendAliasCommands(alias,(CommandSender) e.getPlayer(),sArgs))
            {            
            	e.setCancelled(true);
            }      

        }
	}
}
