package org.bitbucket._newage.betteralias.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NatureEvent extends Event {
	 
	private static final HandlerList handlers = new HandlerList();
	private Player player;
	private boolean cancelled;
	
	public NatureEvent(Player sender) {
		this.player = sender;
	}
	
	public Player getPlayer() {
		return player;
	}
	
    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
